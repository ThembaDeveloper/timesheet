<?php
require_once '../vendor/autoload.php';
require_once '../../vendor/autoload.php';
require_once 'dao/LectureDaoImpl.php';
require_once 'dao/TimesheetDaoImpl.php';
require_once 'model/Lecture.php';
require_once 'model/Timesheet.php';

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

$app = new Silex\Application();
header("Access-Control-Allow-Origin: *");

# lecture facade

$app->GET('/v1/ds03bt/lectures', function (Application $app, Request $request) {

    $dao = new LectureDaoImpl();

    $lectures = $dao->find_all();

    $jsonContent = buildLectureJson($lectures);

    return new Response($jsonContent,Response::HTTP_OK);

});

$app->GET('/v1/ds03bt/lecture/{id}', function (Application $app, Request $request, $id) {

    $dao = new LectureDaoImpl();

    $lecture = $dao->find_lecture_id($id);

    if($lecture == null) {
        return new Response('{ "message": "no lecture found"}',
            Response::HTTP_NOT_FOUND);
    }

    $jsonContent = buildLectureJson($lecture);

    return new Response($jsonContent,
        Response::HTTP_OK);
});

$app->GET('/v1/ds03bt/lecture/{username},{password}', function (Application $app, Request $request, $username, $password) {

    $dao = new LectureDaoImpl();

    $lecture = $dao->find_lecture($username,$password);

    if($lecture == null) {
        return new Response('{ "message": "no lecture found"}',
            Response::HTTP_NOT_FOUND);
    }

    $jsonContent = buildLectureJson($lecture);

    return new Response($jsonContent,
        Response::HTTP_OK);

});

$app->PUT('/v1/ds03bt/lecture/{id}', function (Application $app, Request $request, $id) {

    $dao = new LectureDaoImpl();
    $jsonLecture = $request->getContent();

    // Todo validations
    $lecture = buildLectureObject($jsonLecture);

    $dao->update($id,$lecture->getPassword());

    return new Response("Updated lecture id:".$id,Response::HTTP_OK);
});

$app->POST('/v1/ds03bt/lecture', function (Application $app, Request $request) {

    $dao = new LectureDaoImpl();
    $jsonLecture = $request->getContent();

    // Todo validations
    $lecture = buildLectureObject($jsonLecture);

    $dao->insert($lecture);

    return new Response('New record inserted',Response::HTTP_CREATED);
});

# timesheet facade

$app->GET('/v1/ds03bt/lecture/{lectureId}/timesheets' , function (Application $app, Request $request,$lectureId) {

    $timesheetDao = new TimesheetDaoImpl();

    $timesheet = $timesheetDao->findByLectureId($lectureId);

    if($timesheet == null) {
        return new Response('{ "message": "no timesheet found"}',
            Response::HTTP_NOT_FOUND);
    }

    $jsonContent = buildTimesheetJson($timesheet);

   return new Response($jsonContent, Response::HTTP_OK);
});

$app->GET('/v1/ds03bt/lecture/{lectureId}/timesheet/{date}', function (Application $app, Request $request,$lectureId,$date){

    $timesheetDao = new TimesheetDaoImpl();

    $timesheet = $timesheetDao->findByLectureIdAndDate($lectureId,$date);

    if($timesheet == null) {
        return new Response('{ "message": "no timesheet found"}',
            Response::HTTP_NOT_FOUND);
    }
    $jsonContent = buildTimesheetJson($timesheet);

    return new Response($jsonContent, Response::HTTP_OK);
});

$app->GET('/v1/ds03bt/lecture/timesheet/{timesheetId}', function (Application $app, Request $request,$timesheetId){

    $timesheetDao = new TimesheetDaoImpl();

    $timesheet = $timesheetDao->findByTimesheetId($timesheetId);
    if($timesheet == null) {
        return new Response('{ "message": "no timesheet found"}',
            Response::HTTP_NOT_FOUND);
    }
    $jsonContent = buildTimesheetJson($timesheet);

    return new Response($jsonContent, Response::HTTP_OK);});

$app->POST('/v1/ds03bt/lecture/{lectureid}/timesheet', function (Application $app, Request $request,$lectureid){

    $timesheetObject = buildTimesheetObject($request->getContent());

    $timesheetDao = new TimesheetDaoImpl();

    $timesheetDao->insert($timesheetObject);

    return new Response("created", Response::HTTP_CREATED);
});

$app->PUT('/v1/ds03bt/lecture/{lectureId}/timesheet/{date}', function (Application $app, Request $request,$lectureId,$date){

    $timesheetObject = buildTimesheetObject($request->getContent());

    $timesheetDao = new TimesheetDaoImpl();

    $timesheetDao->updateByDate($lectureId,$date,$timesheetObject->getTasks());

    return new Response("updated", Response::HTTP_CREATED);
});

$app->error(function (Exception $e, $code) {
   return new Response($e);
});

function buildLectureJson($lectureObject){

    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);

    return $serializer->serialize($lectureObject, 'json');
}

function buildLectureObject($jsonObject){

    $encoders = array(new XmlEncoder(),new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);

    return $serializer->deserialize($jsonObject, Lecture::class, 'json');

}

function buildTimesheetJson($timesheetObject){

    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);

    return $serializer->serialize($timesheetObject, 'json');
}

function buildTimesheetObject($jsonObject){

    $encoders = array(new XmlEncoder(),new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);

    return $serializer->deserialize($jsonObject, Timesheet::class, 'json');

}

$app->run();
