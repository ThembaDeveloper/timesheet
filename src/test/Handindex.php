<?php

require('../vendor/autoload.php');

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Application();

$app->get("/lectures", function(){
    return "all lectures";
});

$app->get('/lecture/{id}', function ($id) {
   return "lecture with $id";
});

$app->get('/lecture/{username}/{password}', function($username,$password){
    return "lecture with $username and $password";
});

$app->put('/lecture',function (Request $request) {
    $lecture = $request->get('lecture');

    return new Response("lecture updated $lecture", 201);
});

$app->post('/lecture',function (Request $request) {
    $lecture = $request->get('lecture');

    return new Response("lecture insert $lecture", 201);
});

$app->run();



