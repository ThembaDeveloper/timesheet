<?php
require_once 'ConnFactory.php';
require_once 'LectureDao.php';


    class LectureDaoImpl implements LectureDao
    {
        private $conn ;

        public function __construct() {
            $factory = new connFactory();
            $this->conn = $factory->getConnection();
        }

        public function insert(Lecture $lecture){

            $insert_sql = "INSERT INTO LECTURE (Username,Password) VALUES (?,?)";


            if (!($stmt = $this->conn->prepare($insert_sql))) {
                echo "Prepare failed: (" . $this->conn->errno . ") " . $this->conn->error;
                return FALSE;
            }

            $username = $lecture->getUsername();
            $password = $lecture->getPassword();

            if(!($stmt->bind_param("ss",$username,$password))){
                print("failed to bind");
                return FALSE;
            }

            if(!($stmt->execute())){
                print("failed to execute $stmt->error");
                return FALSE;
            }

            return TRUE;

        }

        public function update(int $id,String $password){
            $insert_sql = "UPDATE LECTURE SET Password = ? WHERE Id = ?";

            if (!($stmt = $this->conn->prepare($insert_sql))) {
                echo "Prepare failed: (" . $this->conn->errno . ") " . $this->conn->error;
                return FALSE;
            }

            if(!($stmt->bind_param("ss",$password,$id))){
                print("failed to bind");
                return FALSE;
            }

            if(!($stmt->execute())){
                print("failed to execute $stmt->error");
                return FALSE;
            }

            return TRUE;
        }

        public function find_lecture(string $username,string $password){

            $select_sql = "SELECT * FROM LECTURE WHERE Username = '$username' AND Password = '$password'";

            $return_value = $this->conn->query($select_sql);

            if ($return_value->num_rows == 0){
                return null;
            }
            return $this->buildArray($return_value)[0];
        }

        public function find_lecture_id(int $id){
            $select_sql = "SELECT * FROM LECTURE WHERE LectureId = $id";

            $result_value = $this->conn->query( $select_sql);

            if ($result_value->num_rows == 0){
                return null;
            }

            return $this->buildArray($result_value)[0];
        }

        public function find_all(){
            // FIXME limit the number of returned object
            $select_sql = 'SELECT * FROM LECTURE';

            $result_value = $this->conn->query($select_sql);

            return $this->buildArray($result_value);
        }

        private function buildArray($result_value){
            $result_value->data_seek(0);

            $array = [];
            for($index = 0;$row = $result_value->fetch_assoc();$index++)
            {
                $lecture = new Lecture();
                $lecture->setLectureId($row["LectureId"]);
                $lecture->setUsername($row["Username"]);
                $lecture->setPassword($row["Password"]);

                $array[$index] = $lecture;
            }

            return $array;
        }

    }