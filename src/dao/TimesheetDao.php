<?php
require_once 'ConnFactory.php';

interface TimesheetDao
{
    public function insert(Timesheet $timesheet);

    public function insertAll($timesheetArray);

    public function update($timesheetId,$tasks);

    public function updateByDate($lectureId,$date,$tasks);

    public function findByLectureId($lectureId,$mondayDate);

    public function findByTimesheetId($timesheetId);

    public function findByLectureIdAndDate($lectureId, $date);

    public function insertPlan($tasks);
}