<?php

interface LectureDao
{
    public function insert(Lecture $lecture);

    public function update(int $id,String $password);

    public function find_lecture(string $username,string $password);

    public function find_lecture_id(int $id);

    public function find_all();

}