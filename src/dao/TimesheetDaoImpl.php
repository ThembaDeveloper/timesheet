<?php

require_once 'TimesheetDao.php';

class TimesheetDaoImpl implements TimesheetDao
{
    // Todo do not run dao without create buildArray

    private $conn;

    public function __construct() {
        $connectionFactory = new connFactory();
        $this->conn = $connectionFactory->getConnection();
    }

    public function insert(Timesheet $timesheet)
    {
        $weekId = 1;

        $insert_sql = "INSERT INTO TIMESHEET (LectureId,PlanId,Date,WeekId) VALUES (?,?,?,?)";
        $planId = $this->insertPlan($timesheet->getTasks());
        $lectureId = $timesheet->getLectureId();
        $day = $timesheet->getDate();

        if (!($stmt = $this->conn->prepare($insert_sql))) {
            echo "Prepare failed: (" . $this->conn->errno . ") " . $this->conn->error;
            return FALSE;
        }

        if(!($stmt->bind_param("iisi",$lectureId,$planId,$day,$weekId))){
            print("failed to bind");
            return FALSE;
        }

        if(!($stmt->execute())){
            print("failed to execute $stmt->error");
            return FALSE;
        }

        return TRUE;
    }

    public function insertAll($timesheetArray){

        foreach($timesheetArray as $timesheet){
            $this->insert($timesheet);
        }

        return true;
    }

    public function update($planId, $tasks)
    {
        $insert_sql = "UPDATE PLAN SET FirstPeriod = ? ,
                                       SecondPeriod = ? ,
                                       ThirdPeriod = ? ,
                                       FourthPeriod = ? ,
                                       FifthPeriod = ? WHERE PlanId = ?";


        if (!($stmt = $this->conn->prepare($insert_sql))) {
            echo "Prepare failed: (" . $this->conn->errno . ") " . $this->conn->error;
            return FALSE;
        }

        if(!($stmt->bind_param("sssssi",$tasks[0],$tasks[1],$tasks[2],$tasks[3],$tasks[4],$planId))){
            print("failed to bind");
            return FALSE;
        }

        if(!($stmt->execute())){
            print("failed to execute $stmt->error");
            return FALSE;
        }

        return TRUE;
    }

    public function updateByDate($lectureId,$date,$tasks){

        $insert_sql = "UPDATE PLAN, TIMESHEET 
                              SET PLAN.FirstPeriod = ? ,
                                  PLAN.SecondPeriod = ?,
                                  PLAN.ThirdPeriod = ? ,
                                  PLAN.FourthPeriod = ? ,
                                  PLAN.FifthPeriod = ?
                              WHERE PLAN.PlanId = TIMESHEET.PlanId AND TIMESHEET.LectureId = ? AND TIMESHEET.Date = ?";


        if (!($stmt = $this->conn->prepare($insert_sql))) {
            echo "Prepare failed: (" . $this->conn->errno . ") " . $this->conn->error;
            return FALSE;
        }

        if(!($stmt->bind_param("sssssis",$tasks[0],$tasks[1],$tasks[2],$tasks[3],$tasks[4],$lectureId,$date))){
            print("failed to bind");
            return FALSE;
        }

        if(!($stmt->execute())){
            print("failed to execute $stmt->error");
            return FALSE;
        }

        return TRUE;

    }

    public function findByLectureId($lectureId, $mondayDate)
    {
        $select_sql = "SELECT LECTURE.LectureId, TIMESHEET.TimesheetId,TIMESHEET.Date, PLAN.FirstPeriod, PLAN.SecondPeriod,PLAN.ThirdPeriod , PLAN.FourthPeriod, PLAN.FifthPeriod
                      FROM (((LECTURE INNER JOIN TIMESHEET ON LECTURE.LectureId=TIMESHEET.LectureId)
                      INNER JOIN PLAN ON TIMESHEET.PlanId=PLAN.PlanId)
                      INNER JOIN WEEK ON TIMESHEET.weekId=WEEK.weekId)
                      WHERE LECTURE.LectureId = $lectureId AND WEEK.MondayDate = '$mondayDate'";

        $return_value = $this->conn->query($select_sql);

        if ($return_value->num_rows == 0){
            return null;
        }
        return $this->buildArray($return_value);
    }

    public function findByTimesheetId($timesheetId)
    {
        $select_sql = "SELECT TIMESHEET.LectureId, TIMESHEET.TimesheetId, TIMESHEET.Date, PLAN.FirstPeriod, PLAN.SecondPeriod,
                                             PLAN.ThirdPeriod, PLAN.FourthPeriod, PLAN.FifthPeriod
                        FROM TIMESHEET
                        INNER JOIN PLAN ON TIMESHEET.PlanId=PLAN.PlanId
                        WHERE TIMESHEET.TimesheetId = '$timesheetId'";

        $return_value = $this->conn->query($select_sql);

        if ($return_value->num_rows == 0){
            return null;
        }
        return $this->buildArray($return_value)[0];
    }

    public function findByLectureIdAndDate($lectureId, $date)
    {
        $select_sql = "SELECT LECTURE.LectureId, TIMESHEET.TimesheetId, TIMESHEET.Date, PLAN.FirstPeriod, PLAN.SecondPeriod,
                                                        PLAN.ThirdPeriod, PLAN.FourthPeriod, PLAN.FifthPeriod
                        FROM ((LECTURE
                        INNER JOIN TIMESHEET ON LECTURE.LectureId=TIMESHEET.LectureId)
                        INNER JOIN PLAN ON TIMESHEET.PlanId=PLAN.PlanId)
                        WHERE LECTURE.LectureId = '$lectureId' AND TIMESHEET.Date = '$date'";

        $return_value = $this->conn->query($select_sql);

        if ($return_value->num_rows == 0){
            return null;
        }
        return $this->buildArray($return_value)[0];
    }

    public function insertPlan($tasks)
    {
        $insert_sql = "INSERT INTO PLAN (FirstPeriod,SecondPeriod,ThirdPeriod,FourthPeriod,FifthPeriod) VALUES (?,?,?,?,?)";

        if (!($stmt = $this->conn->prepare($insert_sql))) {
            echo "Prepare failed: (" . $this->conn->errno . ") " . $this->conn->error;
            return FALSE;
        }

        if(!($stmt->bind_param("sssss",$tasks[0],$tasks[1],$tasks[2],$tasks[3],$tasks[4]))){
            print("failed to bind");
            return FALSE;
        }

        if(!($stmt->execute())){
            print("failed to execute $stmt->error");
            return FALSE;
        }

        return mysqli_insert_id($this->conn);
    }

    private function buildArray($result_value){
            $result_value->data_seek(0);

            $array = [];
            for($index = 0;$row = $result_value->fetch_assoc();$index++)
            {

                $timesheet = new Timesheet();
                $timesheet->setId($row["TimesheetId"]);
                $timesheet->setLectureId($row["LectureId"]);
                $timesheet->setDate($row["Date"]);
                $timesheet->setTasks(array($row["FirstPeriod"],$row["SecondPeriod"],
                                           $row["ThirdPeriod"],$row["FourthPeriod"],$row["FifthPeriod"]));

                $array[$index] = $timesheet;
            }

            return $array;
    }
}