<?php

class Lecture
{

    private $lectureId;
    private $username;
    private $password;

    public function getLectureId(){
        return $this->lectureId;
    }

    public function setLectureId($lectureId){
        $this->lectureId = $lectureId;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function toJson() {
        return "{
            \"id\" :$this->lectureId,
            \"username\":\"$this->username\",
            \"password\":\"$this->username\"
        }";
    }


}