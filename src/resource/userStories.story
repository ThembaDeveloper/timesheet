Meta: DSO34BT registration system

Narrative:
As lecturer
I want to login
So that I can submit/view timesheet

Scenario: lecture login
Given I am lecture login
When I enter my username and password
Then I should see week timesheet

Scenario: lecture without account
Given I am lecture and I do not have account
When I Enter my username and password
Then I should go to register page

Scenario: lecture register
Given I am lecture and I want to register
When I enter my details
Then I should see empty week timesheet

Narrative:
As lecturer
I want to view timesheet
So that I can update it

Scenario: lecture viewing timesheet
Given I am lecture wanting to view timesheet
When I enter date range
Then I should get timesheet between the range


Scenario: lecture updating timesheet
Given I am lecture wanting to update timesheet
When I enter task on time slot
Then the task get saved


Narrative:
As administrator
I want to look for lecturers
So that I can view a lecturer timesheet

Scenario: administrator looking for lectures
Given I am administrator wanting to see lectures
When I enter subject,Name,lecture number
Then I should get a list of lectures


Scenario: administrator viewing lecture timesheet
Given I am administrator wanting to view timesheet
When I enter date range
Then I should get lecture timesheet between the range
