var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        ajax: "../php/staff.php",
        table: "#example",
        fields: [ {
            label: "Monday:",
            name: "Monday"
        }, {
            label: "Tuesday:",
            name: "Tuesday"
        }, {
            label: "Wednesday:",
            name: "Wednesday"
        }, {
            label: "Thursday:",
            name: "Thursday"
        }, {
            label: "Friday:",
            name: "Friday"
        }
        ]
    } );

    // Activate an inline edit on click of a table cell
    $('#example').on( 'click', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this, {
            buttons: { label: '&gt;', fn: function () { this.submit(); } }
        } );
    } );

    $('#example').DataTable( {
        dom: "Bfrtip",
        ajax: "../php/staff.php",
        columns: [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            { data: "Monday" },
            { data: "Tuesday" },
            { data: "Wednesday" },
            { data: "Thursday" },
            { data: "Friday" }
        ],
        order: [ 1, 'asc' ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        buttons: [
            { extend: "create", editor: editor },
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: editor }
        ]
    } );
} );